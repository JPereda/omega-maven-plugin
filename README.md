# Maven plugin for JavaFX

Maven plugin to run JavaFX 11+ applications

## Install

Clone the project, set JDK 11 and run

```
mvn install
``` 